<?php
/**
 * @file
 * Contains \Drupal\loginlogout\Form\LoginLogoutSettingsForm.
 */
namespace Drupal\loginlogout\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure book settings for this site.
 */
class LoginLogoutSettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'loginlogout_admin_settings';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('loginlogout.settings');
    $form['loginlogout_urls'] = array(
      '#type' => 'textarea',
      '#title' => t('Urls for login logout forms.'),
      '#default_value' => $config->get('loginlogout_urls'),
      '#description' => $this->t('One per line, Lists URLS that should have destination added to them (or remove URLS that should not)'),
      '#required' => TRUE,
    );
    $form['array_filter'] = array('#type' => 'value', '#value' => TRUE);

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $child_type = $form_state->getValue('loginlogout_urls');
    if (empty($form_state->getValue('loginlogout_urls'))) {
      $this->setFormError('loginlogout_urls', $form_state, $this->t('Field should not be left blank.'));
    }

    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $allowed_urls = $form_state->getValue('loginlogout_urls');
    $this->config('loginlogout.settings')
      ->set('loginlogout_urls', $allowed_urls)
      ->save();
    parent::submitForm($form, $form_state);
  }

}
